import { useState, useEffect, useContext} from "react";
import { Navigate } from "react-router-dom";
import { Button, Form, Row, Col } from "react-bootstrap";
import Swal from 'sweetalert2'
import UserContext from "../UserContext";


export default function Login (props) {

    //allows us to consume the User context object and its properties to use for user validation
    const {user,setUser} = useContext(UserContext);
     console.log(user)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);




    function loginUser(e){
        e.preventDefault ();


        fetch('http://localhost:4000/users/login', {

            method : "POST",
            headers : {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (typeof data.access !== "undefined"){
                localStorage.setItem('token',data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title : 'Login Successful',
                    icon : "success",
                    text : "Welcome to Zuitt"
                })
            }
            else{
                Swal.fire({
                    title : 'Authentication Failed',
                    icon : "error",
                    text : "Please check you credentials"
                })
            }
        })

        // localStorage.setItem("email", email)

        //to access the user information, it can be done using localstorage, this is necessary to update the user state which will help the App component and rerender it to avoid refreshing the page upon user login and logout

        //when state change components are rerendered and the AppNavbar component will be updated based on the user credentials

        // setUser({
        //     email: localStorage.getItem('email')
        // })

        //clear input fields
        setEmail('');
        setPassword('');


        const retrieveUserDetails = (token) => {
            fetch('http://localhost:4000/users/details',{

                method: "POST",
                headers : {
                    Authorization : `Bearer ${token}`
                }
            })
            .then (res => res.json())
            .then(data => {
                console.log(data)

                setUser ({
                    id: data._id,
                    isAdmin : data.isAdmin
                })
            })
        };

        
        // alert(`${email} has been verfied, Welcome back!`)
    }

    useEffect (() => {

        if(email !== '' && password!=='')
        {
            setIsActive(true)
        }
        else{
            setIsActive(false)
        }

    }, [email, password])

    return (
        (user.id !== null) ?
         <Navigate to="/courses"/>

        :

        <Row>
            <Col className="mx-auto" xs={12} md="6">
                <Form onSubmit={e => loginUser(e)}>
                <h1>Login</h1>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder = "Enter your Email Here"
                        value= {email}
                        onChange={ e => setEmail(e.target.value)}
                        required
                    />
                    
                </Form.Group>
                
                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder = "Input your password Here"
                        value= {password}
                        onChange={ e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                    {   
                        isActive ?

                            <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-3 loginBtn">Login</Button>
                        :
                            <Button variant="secondary" type="submit" id="submitBtn" className="mt-3 mb-3 loginBtn" disabled>Login</Button>
                    }
            </Form>
        </Col>
       </Row>
    )
};