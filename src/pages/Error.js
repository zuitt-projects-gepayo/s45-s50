import { Row,Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Error (){

    return(

        <Row>
            <Col>
                <h1>Error 404</h1>
                <p>Go back to <Link to="/">Hompage</Link></p>
            </Col>
        </Row>
    )
};