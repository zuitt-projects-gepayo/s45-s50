import { Button, Row, Col } from "react-bootstrap";
import {Link} from 'react-router-dom';


export default function Banner () {
    return(

        <Row>
            <Col>
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere</p>
                <Link as={Link} to="/register"><Button variant="primary">Enroll Now!</Button></Link>
            </Col>
        </Row>
    )
}