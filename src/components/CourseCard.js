import {useState, useEffect} from 'react';
import {Card, Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default function CourseCard({courseProp}) {

    console.log(courseProp);
        // expected result is coursesData[0]
    console.log(typeof courseProp);
        // result: object

    // const [count, setCount] = useState(0);
    // const [seats, setSeat] = useState(30);
    // const [isOpen, setIsOpen] = useState(true);
    // // syntax: const [getter, setter] = useState(initialValueOfGetter)

    // function enroll(){


    //     if( count !== 30 && seats !== 0)
    //     {  
    //         setCount(count + 1)
    //         console.log('Enrollees' + count)
    
    //         setSeat (seats - 1)
    //         console.log(seats)
    //     }

    // };

    // useEffect(()=> {
    //     if(seats === 0){
            
    //         alert("No more Seats Available")
    //     }
    // }, [seats])
    //SYNTAX: useEffect (() => {}, [])



    const { _id, name, description, price} = courseProp




    return (
        <Row className="mt-3 mb-3">
            <Col>
                <Card>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Button variant="primary" as={Link} to ={`/courses/${_id}`}>See Details</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
