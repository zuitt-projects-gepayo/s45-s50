import { Card, Col, Row } from "react-bootstrap";

export default function Highlights () {
    return(
        <Row className="mt-3 mb-3">
            <Col xs={12} md="4">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo, laborum ipsum dolore repellat quasi dignissimos tenetur. Aliquam sint pariatur, numquam quo suscipit accusantium repellendus ipsum ut! Debitis facere voluptate expedita!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md="4">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo, laborum ipsum dolore repellat quasi dignissimos  voluptate expedita!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md="4">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo, laborum ipsum dolore repellat quasi dignissimos tenetur. Aliquam sint pariatur, numquam quo suscipit accusantium repellendus ipsum ut! Debitis facere voluptate expedita!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}